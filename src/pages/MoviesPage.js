import React, { useEffect } from 'react'
import { Alert, MovieCard, Skeleton } from '../components'
import { useDynamicTitle, useFetch } from '../hooks';

export const MoviesPage = ({apiPath, title}) => {
  useDynamicTitle(title,'Film Fiesta');
  const BASE_URL = process.env.REACT_APP_BASE_URL;
  const API_KEY = process.env.REACT_APP_API_KEY;
  const {data, isLoading, error, setUrl} = useFetch();
  const renderSkeletons = (nums = 3) => {
    const skeletons = [];
    for(let i=0;i<nums;i++){
      skeletons.push(<Skeleton key={i}/>);
    }
    return skeletons;
  }
  
  useEffect( () => {
    const URL = `${BASE_URL}/3/movie/${apiPath}?api_key=${API_KEY}`;
    // console.log(URL);
    setUrl(URL);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[apiPath]);
  
  return (
    <main>
      <div className='flex justify-start flex-wrap'>
        { error && <Alert/>}
        { isLoading && renderSkeletons(6)}
        { data && data.results && data.results.map(movie => (<MovieCard key={movie.id} movie={movie}/>))}
      </div>
    </main>
  )
}
