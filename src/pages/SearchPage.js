import React, { useEffect } from 'react'
import { useDynamicTitle, useFetch } from '../hooks';
import { MovieCard, Skeleton } from '../components';
import { useSearchParams } from 'react-router-dom';

export const SearchPage = () => {
    const [params] = useSearchParams();
    const queryString = params.get('q');
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    const API_KEY = process.env.REACT_APP_API_KEY;
    
    // eslint-disable-next-line no-unused-vars
    const {data, isLoading, error, setUrl} = useFetch();

    useDynamicTitle(`Search Results for ${queryString}`,'Film Fiesta');
    const renderSkeletons = (nums = 3) => {
        const skeletons = [];
        for(let i=0;i<nums;i++){
            skeletons.push(<Skeleton key={i}/>);
        }
        return skeletons;
    }
    
    useEffect(() => {
        const URL = `${BASE_URL}/3/search/movie?api_key=${API_KEY}&query=${queryString}`;
        setUrl(URL);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[queryString]);
    
    return (
      <main>
        <h1 className='text-3xl dark:text-white text-slate-800'>
            { data && data.results.length === 0 ?  `No movies found for ${queryString}` : `Search results for ${queryString}`}  
        </h1>
        <div className='flex justify-start flex-wrap'>
          { isLoading && renderSkeletons(6)}
          { data && data.results && data.results.map(movie => (<MovieCard key={movie.id} movie={movie}/>))}
        </div>
      </main>
    )
}
