import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { MovieDetailsPage, MoviesPage, NotFoundPage, SearchPage } from '../pages'

export const AppRoutes = () => {
  return (
    <>
    <Routes>
        <Route path='/' element={<MoviesPage key="now_playing" title={"Now Playing | Film Fiesta"} apiPath={'/now_playing'}/>} />
        <Route path='/movies/:id' element={<MovieDetailsPage/>} />
        <Route path='/movies/search' element={<SearchPage/>} />
        <Route path='/top-rated' element={<MoviesPage key="top_rated" title={"Top Rated | Film Fiesta"} apiPath={'/top_rated'}/>} />
        <Route path='/upcoming' element={<MoviesPage key="upcoming" title={"Upcoming | Film Fiesta"} apiPath={'/upcoming'}/>} />
        <Route path='/popular' element={<MoviesPage key="popular" title={"Popular | Film Fiesta"} apiPath={'/popular'}/>} />
        <Route path='*' element={<NotFoundPage title={"Page Not Found | Film Fiesta"}/>} />
    </Routes>
    </>
  )
}
